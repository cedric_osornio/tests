package tests;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.text.html.MinimalHTMLWriter;
public class Number {
	private Random rnd = new Random();
	private String res="";
	private int temp;
	private int[] t;
	private int j;
	public int GetNumber(){
		return 0;
	}
	private BigDecimal resultt = BigDecimal.ONE;	
	public BigDecimal factorial(int n){
		  if (n==0){
		  return BigDecimal.ONE;
		  }
		  for (long i=1;i<=n;i++){			   
		    resultt = resultt.multiply(BigDecimal.valueOf(i));
		  }
		  return resultt;
		}
	private BigDecimal pi= BigDecimal.ZERO;
	public BigDecimal getPi(long presision){
		  for (int i=0;i<presision;i++){
		    pi=pi.add(factorial(4*i).multiply(BigDecimal.valueOf(1103+26390*i)).divide((factorial(i).pow(4)).multiply(BigDecimal.valueOf(396).pow(4*i)),100,BigDecimal.ROUND_HALF_UP));
		  }
		  return pi;
		}
	
	public String GetNumber(int input){
		switch (input) {
		case 1:
			res="uno";
			break;

		case 2:
			res="dos";
			break;
		case 3:
			res="tres";
			break;
		}
		return res;
	}
	public boolean isequal (double d1, double d2){
		if (Math.abs(d1-d2)<0.001){
			return true;
		}
		return false;
		
	}
	public boolean isMultipleOf(int a,int b){
		if (a%b==0){
			return true;
		}
		return false;
	}
	public boolean isMultipleOf(long a,long b){
		if (a%b==0){
			return true;
		}
		return false;
	}
	public long aFactorOf(int a){
		for (int i=2;i<a;i++){
			if (a%i==0){
				return i;
			}
		}
		return -1;
	}
	public long aFactorOf(long a){
		for (long i=2;i<a;i++){
			if (a%i==0){
				return i;
			}
		}
		return -1;
	}
	public int[] result = new int[10];
	public int[] generateTable(){
		for (int i=0;i<10;i++){
			result[i]=Math.abs(rnd.nextInt());
		}		
		return result;	
	}
	public int [] receiveTable(int table[]){
		t= table;
		return quickSort(1, 8);
	}
	private int partition (int g,int d){
		System.out.println("YA entre 3");
		int key = t[g];
		int i = g+1;
		j=d;
		while(i<=j){
			while (i<=j && t[i]<=key){
				i++;
			}
			while (key<t[i]){
				j--;
			}
			if (i<j){
				temp=t[i];
				t[i]=t[j];
				t[j]=temp;
				i++;
				j--;
			}			
		}
		temp=t[g];
		t[g]=t[j];
		t[j]=temp;
		System.out.println("YA sali");
		return j;
	}
	private int[] quickSort(int g,int d){
		System.out.println("Ya entré");
		if (g<d){
			System.out.println("Ya entré 2");
			g=partition(g,d);
			quickSort(g, j-1);
			quickSort(j+1,d);
			
		}		
		return t;
	}
	private int fi1 =1;
	private int fi2=1;	
	public int fibo(int n){
		if (n==1 || n==2){
			return 1;
		}
		int res1=1;
		int res2=1;
		for (int i=0;i<n;i++){
			res2+=res1;
			res1=res2;
			System.out.println(res2);
		}
		return res2;		
	}
	//ArrayList<ArrayList<Double>> matrix = new ArrayList<ArrayList<Double>>();
	ArrayList<Double> list = new ArrayList<Double>();
	public void findBestApproximation(double input, int interval){
		int indice=0;
		for (int i=1;i<interval;i++){
			for (int j=1;j<interval;j++){
				list.add((double) i/j);
			}						
			}
		double minimum=100;
		indice=0;
		for (int i=0;i<list.size();i++){
			double epsilon =Math.abs(list.get(i)-input);
			if (epsilon<minimum){
				minimum=epsilon;
				System.out.println(list.get(i));
				indice=i;
			}			
		}
		int numerator = (int) indice / (interval-1) +1;
		int denumerator = indice % (interval-1) +1 ;
		System.out.println(numerator);
		System.out.println(denumerator);
		
	}
}
