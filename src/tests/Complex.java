package tests;

public class Complex {
	private double real=0;
	private double imaginary=0;
	
	
	public  Complex(double real,double imaginary){
		this.real=real;
		this.imaginary=imaginary;
	}
	
	
	public void toConsole(){
		if (imaginary<0){
			System.out.println(real+" - "+Math.abs(imaginary)+"i");
		}
		else{
			System.out.println(real+" + "+imaginary+"i");
		}		
	}
	
	
	public double module(){
		return Math.sqrt(Math.pow(real, 2)+Math.pow(imaginary, 2));
	}
	
	
	public double angleRadiants(){
		double costheta=real/module();
		double sintheta=imaginary/module();
		double angle=Math.acos(Math.abs(costheta));
		if (costheta<0 && sintheta<0){			
			return angle+Math.PI;
		}
		else if (costheta<0 && sintheta>0){			
			return angle+Math.PI/2;
		}
		else if(costheta>0 && sintheta<0){			
			return -angle;
		}					
		return angle;			
	}
	
	public double angleDegrees(){
		return angleRadiants()*180/Math.PI;
	}
	
	
	public Complex conjugate(){
		Complex conju = new Complex(real, -imaginary);
		return conju;
	}
	
	public double getReal(){
		return this.real;
	}
	
	public double getImaginary(){
		return this.imaginary;
	}
	
	
	public Complex add(Complex z){
		double r=z.real+this.real;
		double i=z.imaginary+this.imaginary;
		Complex res= new Complex (r,i);
		return res;
	}
	
	
	public Complex multiply (Complex z){
		double a=z.getReal()*this.real-z.getImaginary()*this.imaginary;		
		double b=z.getReal()*this.imaginary+z.getImaginary()*this.real;
		Complex res= new Complex (a,b);
		return res;
	}
}
